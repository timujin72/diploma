output "internal_ip_address_vm_1" {
  value = module.ya_instance_1.internal_ip_address_vm
}

output "external_ip_address_vm_1" {
  value = module.ya_instance_1.external_ip_address_vm
}

output "internal_ip_address_vm_2" {
  value = module.ya_instance_2.internal_ip_address_vm
}

output "external_ip_address_vm_2" {
  value = module.ya_instance_2.external_ip_address_vm
}

output "internal_ip_address_vm_3" {
  value = module.ya_instance_2.internal_ip_address_vm
}

output "external_ip_address_vm_3" {
  value = module.ya_instance_2.external_ip_address_vm
}
# output "target_group_id" {
#   value = yandex_lb_target_group.foo.id
# }