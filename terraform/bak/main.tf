terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">=0.80"
    }
  }

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "tf-state-bucket-my1"
    region     = "ru-central1-a"
    key        = "issue1/lemp.tfstate"
    access_key = "YCAJEBZP-t7Np1mgdHRvigkEn"
    secret_key = "YCN5viBJ09QvPtEXsX4PBfI9T4gs5adjrXvBkjgm"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token = "t1.9euelZqVlYyNlozLyIrPi5iKk5mNz-3rnpWaxpGelMzHnY3JksvNlInLjczl8_dKcmhc-e83awkl_N3z9wohZlz57zdrCSX8.BZGzQjAzmEwAc_b2S50lAO9Ho_ZG6L-C79a3GedW1hjfjyckX8Jmm5DuWz3XW7JX4WHkU9l7g7kaes-MBPhbAQ"
  cloud_id  = "b1g52fls4gn42ltpb8r4"
  folder_id = "b1gkah722jo1350jlth2"
  zone      = "ru-central1-a"
}

resource "yandex_vpc_network" "network" {
  name = "network"
}

resource "yandex_vpc_subnet" "subnet1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

# resource "yandex_vpc_subnet" "subnet2" {
#   name           = "subnet2"
#   zone           = "ru-central1-a"
#   network_id     = yandex_vpc_network.network.id
#   v4_cidr_blocks = ["192.168.11.0/24"]
# }


module "ya_instance_1" {
  source                = "./modules/instance"
  instance_family_image = "ubuntu-2204-lts"
  vpc_subnet_id         = yandex_vpc_subnet.subnet1.id
}

module "ya_instance_2" {
  source                = "./modules/instance"
  instance_family_image = "ubuntu-2204-lts"
  vpc_subnet_id         = yandex_vpc_subnet.subnet1.id
}

module "ya_instance_3" {
  source                = "./modules/instance"
  instance_family_image = "ubuntu-2204-lts"
  vpc_subnet_id         = yandex_vpc_subnet.subnet1.id
}

# resource "yandex_lb_target_group" "foo" {
#   name      = "my-target-group1"
#   region_id = "ru-central1"

#   target {
#     subnet_id = yandex_vpc_subnet.subnet1.id
#     address   = module.ya_instance_1.internal_ip_address_vm
#   }

#   target {
#     subnet_id = yandex_vpc_subnet.subnet2.id
#     address   = module.ya_instance_2.internal_ip_address_vm
#   }
# }
# resource "yandex_lb_network_load_balancer" "foo" {
#   name = "my-networkbalancer-1"
#   listener {
#     name = "listener1"
# 	 port = 80
#     external_address_spec {
#       ip_version = "ipv4"
#     }
#   }
#   attached_target_group {
#     target_group_id = yandex_lb_target_group.foo.id
#     healthcheck {
#       name = "healthcheckname1"
#         http_options {
#           port = 80
#           path = "/"
#         }
#     }
#   }
#}