# Определение провайдера Яндекс.Облако

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone      = "ru-central1-a"
  cloud_id  = "b1g52fls4gn42ltpb8r4"
  folder_id = "b1gkah722jo1350jlth2"
  token = "t1.9euelZqVlYyNlozLyIrPi5iKk5mNz-3rnpWaxpGelMzHnY3JksvNlInLjczl8_dKcmhc-e83awkl_N3z9wohZlz57zdrCSX8.BZGzQjAzmEwAc_b2S50lAO9Ho_ZG6L-C79a3GedW1hjfjyckX8Jmm5DuWz3XW7JX4WHkU9l7g7kaes-MBPhbAQ"
}


# Определение виртуальной сети
resource "yandex_vpc_network" "network" {
  name = "my-vpc-network"
}

# Определение подсети
resource "yandex_vpc_subnet" "subnet" {
  name           = "my-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = ["10.0.0.0/24"]
}


# Определение группы доступа
# resource "yandex_compute_instance_group" "instance_group" {
#   name        = "my-instance-group"
#   description = "My instance group"
#   zone        = "ru-central1-a"
# }

# Определение серверов
resource "yandex_compute_instance" "server" {
  count                  = 3
  name                   = "server-${count.index + 1}"
  zone                   = "ru-central1-a"
  platform_id            = "standard-v2"
  resources {
    cores  = 2
    memory = 4
  }
  boot_disk {
    initialize_params {
      image_id = "fd82sqrj4uk9j7vlki3q"
    }
  }
  network_interface {
    subnet_id      = yandex_vpc_subnet.subnet.id
    nat            = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
 # instance_group_id = yandex_compute_instance_group.instance_group.id
}

